package xdecoder.java.youtubeapi;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class YoutubeInterface 
{
    private static final String API_KEY = "AIzaSyD3-wBJqiCxPcPKbCBgivt5od7euTXwsE4";
    
    public static String getVideoID(String keyword) throws Exception
    {
        keyword = keyword.replaceAll(" ", "+");

        String url = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&order=rating&q="+keyword+"&key="+API_KEY;
        
        Document doc = Jsoup.connect(url).ignoreContentType(true).timeout(10 * 1_000).get();

        String jsonData = doc.text();//System.out.println(jsonData);
        
        /*JSONObject jsonObject = new JSONObject(getJson).getJSONObject("items").getJSONObject("id");

        String videoID = (jsonObject.getString("videoId"));*/

        jsonData = jsonData.replaceAll(" ", "");
        
        String key = "\"videoId\":\"";
        
        String videoID = jsonData.split(key)[1].split("\"")[0];
        
        return videoID;
    }
}
