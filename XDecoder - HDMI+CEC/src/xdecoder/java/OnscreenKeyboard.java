package xdecoder.java;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

/**
 *
 * @author TDavid
 */

public class OnscreenKeyboard extends TilePane
{
    private final String[] KEYS =
    {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "M"};
    
    public static boolean isMouseFocused;
    
    private TextField FIELD;
    
    public OnscreenKeyboard()
    {
        setOnMouseEntered((MouseEvent e)->{
            isMouseFocused = true;
        });
        
        setOnMouseExited((MouseEvent e)->{
            isMouseFocused = false;
        });
        
        setAlignment(Pos.TOP_LEFT);
        //setHgap(20);
        //setVgap(20);
        
        setStyle("-fx-background-color: black;");
        
        setMinWidth(400);
        setMaxWidth(400);
        
        for(String key: KEYS)
        {
            Label label = new Label(key);
            
            label.setStyle("-fx-border-width: 2px; -fx-border-color: white; -fx-padding: 20, 0, 20, 0;");
            label.setOnMouseClicked((MouseEvent event)->{FIELD.appendText(key);});
            label.setTextFill(Paint.valueOf(Color.WHITE.toString()));
            label.setFont(Font.font("monospace", 20));
            
            getChildren().add(label);
        }
        
        Label space = new Label("SB>");
        space.setStyle("-fx-border-width: 2px; -fx-border-color: white;       ");
        space.setOnMouseClicked((MouseEvent event)->{FIELD.appendText(" ");});
        space.setTextFill(Paint.valueOf(Color.WHITE.toString()));
        space.setFont(Font.font("bold", 30));
        
        Label del = new Label("DEL");
        del.setOnMouseClicked((MouseEvent event) ->
        {FIELD.setText(FIELD.getText().substring(0, FIELD.getText().length()-1)); FIELD.positionCaret(FIELD.getText().length());});
        del.setStyle("-fx-border-width: 2px; -fx-border-color: white;       ");
        del.setTextFill(Paint.valueOf(Color.WHITE.toString()));
        del.setFont(Font.font("bold", 30));
        
        Label close = new Label("<X>");
        close.setStyle("-fx-border-width: 2px; -fx-border-color: white;        ");
        close.setOnMouseClicked((MouseEvent event)->{Main.ME.hideKeyboard();});
        close.setTextFill(Paint.valueOf(Color.RED.toString()));
        close.setFont(Font.font("bold", 30));
        
        getChildren().addAll(space, del, close);
    }
    
    public void setTextField(TextField field)
    {
        FIELD = field;
    }
    
    public TextField getTextField()
    {
        return FIELD;
    }
}
