/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package xdecoder.java;

import com.sun.glass.ui.Application;
import com.sun.glass.ui.Robot;
import javafx.scene.layout.Region;

/**
 *
 * @author TDavid
 */

public class Cursor extends Region
{
    //private static final Robot ROBOT = Application.GetApplication().createRobot();
    
    //private static int X, Y;
    
    public static final int MOUSE_SIZE = 32;
    
    public Cursor()
    {
        setStyle("-fx-background-image: url('/xdecoder/assets/cursor.png'); -fx-background-size: 100% 100%;");
        
        Robot robo = Application.GetApplication().createRobot();
        
        setMinSize(MOUSE_SIZE, MOUSE_SIZE);
        setMaxSize(MOUSE_SIZE, MOUSE_SIZE);
        setLayoutX(robo.getMouseX());
        setLayoutY(robo.getMouseY());
        setMouseTransparent(true);
    }
    
    public void update(int X, int Y)
    {
       setLayoutX(X);
       setLayoutY(Y); 
    }
}