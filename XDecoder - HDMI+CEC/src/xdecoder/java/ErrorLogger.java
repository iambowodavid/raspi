/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xdecoder.java;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Date;

/**
 *
 * @author Bowofoluwa Okeowo
 */
public class ErrorLogger 
{
    public static void log(Throwable t)
    {
        try(FileOutputStream fos = new FileOutputStream(Utils.ROOT_DIRECTORY+"/error.log", true); PrintStream printer = new PrintStream(fos);)
        {
            printer.println("\n\n"+new Date().toString()+">>>>");
            
            t.printStackTrace(printer);
            t.printStackTrace();
        }catch(Exception e){t.printStackTrace(); e.printStackTrace();}
    }
    
    public static void severeLog(Throwable t)
    {
        log(t);
        
        try 
        {
            Runtime.getRuntime().exec("sudo reboot");
        } catch (Exception e){log(t); Utils.terminate(1);}
    }
}
