/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xdecoder.java;

/**
 *
 * @author TDavid
 */
public class Strings 
{
    public static final String FAILED_SEARCH_QUERY = "An error occurred and the search query could not be processed!";
    
    public static final String SEARCH_QUERY_BEING_PROCCESSED = "A search query is being processed.";
    
    public static final String NO_NETWORK_CONNECTION = "Error: No network connection detected.\nPlease utilise a compatible network module.";
}
