/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xdecoder.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * "D:\-WINDOWS_DATA\ProgramFiles\Mozilla Firefox\firefox.exe" | sudo epiphany-browser -a --profile /root/.config
 * taskkill /F /IM firefox.exe | sudo killall epiphany-browser
                                
 *
 * @author TDavid
 */

public final class Utils
{
    public static final File ROOT_DIRECTORY = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getFile()).getParentFile();
    
    public static String getProcessOutput(Process proc) throws IOException
    {
        String str = "";
        
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));)
        {
            while(proc.isAlive())
            {
                str += reader.readLine();
            }
        }
        
        return str;
    }
    
    public static String readResourceAsString(String resourceName) throws IOException
    {
        byte[] data;
        
        try (InputStream is = Utils.class.getResourceAsStream(resourceName)) 
        {
            data = new byte[is.available()];
            
            is.read(data);
        }
        
        return new String(data);
    }
    
    private static /*temporary*/ boolean isNet;
    public static boolean isNetworkConnected(boolean showProgressBar)
    {if(isNet){return isNet;}
    
        boolean ans = false; 
        
        try
        {
            Socket s = new Socket("52.37.119.93", 80);
            
            s.setSoTimeout(4_000);
            s.getInputStream().close();
            
            ans = true;
            
            isNet = true;
        }catch(Exception e){ErrorLogger.log(e);}
            
        return ans;
    }
    
    public static void terminate(int status)
    {
        //Cleanup
        try 
        {
            Main.ME.stopStartupSound();
            GPIOControl.destroy();
        } catch (Throwable e) {ErrorLogger.log(e);}
        finally
        {
            Runtime.getRuntime().exit(status);
        }
    }
}