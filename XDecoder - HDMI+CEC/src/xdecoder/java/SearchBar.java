/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xdecoder.java;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import xdecoder.java.youtubeapi.YoutubeInterface;

/**
 *
 * @author TDavid
 */

public class SearchBar extends AnchorPane
{
    private final TextField INPUT_BOX = new TextField();
    
    private boolean isSetup;
    
    @Override
    protected void layoutChildren() 
    {
        if(!isSetup)
        {
            setup();
            
            isSetup = true;
        }
        
        super.layoutChildren();
    }
    
    private void setup()
    {
        Label search_btn = new Label();
        
        search_btn.setStyle("-fx-background-image: url('/xdecoder/assets/search.jpg'); -fx-background-size: 100% 100%;");
        search_btn.setMinSize(60, getPrefHeight());
        AnchorPane.setLeftAnchor(search_btn, 0.0);
        search_btn.setOnMouseClicked((MouseEvent event) ->
        {
            String str = INPUT_BOX.getText();
            
            if(!str.isEmpty())
            {
                Runnable runnable = () -> 
                {
                    try 
                    {   
                        if(Utils.isNetworkConnected(true))
                        {
                            Main.ME.toast(Strings.SEARCH_QUERY_BEING_PROCCESSED, 15_000);
                            
                            Main.ME.playVideo(YoutubeInterface.getVideoID(str));
                        }
                        else
                        {
                            Main.ME.toast(Strings.NO_NETWORK_CONNECTION, null);
                        }
                    } catch (Exception ex){ErrorLogger.log(ex); Main.ME.toast(Strings.FAILED_SEARCH_QUERY, null);}
                };
                
                new Thread(runnable).start();
            }
        });
        
        INPUT_BOX.setOnMouseClicked((MouseEvent event) -> {Main.ME.showKeyboard(INPUT_BOX);});
        INPUT_BOX.setMinSize(getPrefWidth() - 70, getPrefHeight());
        INPUT_BOX.setPromptText("Search for youtube videos");
        AnchorPane.setRightAnchor(INPUT_BOX, 1.0);
        INPUT_BOX.setFocusTraversable(false);
        
        getChildren().addAll(search_btn, INPUT_BOX);
    }
}
