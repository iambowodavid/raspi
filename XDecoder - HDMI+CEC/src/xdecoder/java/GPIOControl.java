/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xdecoder.java;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

/**
 *
 * @author TDavid
 */

public class GPIOControl 
{
    private final static GpioController GPIO = GpioFactory.getInstance();
    
    public static void destroy()
    {
        GPIO.shutdown();
    }
    
    public static class PIN1
    {
        private static final GpioPinDigitalOutput PIN = GPIO.provisionDigitalOutputPin(RaspiPin.GPIO_01, PinState.LOW);
    
        public static void pinBlink(int duration, int period)
        {
            PIN.blink(duration, period);
        }
        
        public static void pinHigh() 
        {
            PIN.high();
        }

        public static void pinLow() 
        {
            PIN.low();
        }
    }
}
