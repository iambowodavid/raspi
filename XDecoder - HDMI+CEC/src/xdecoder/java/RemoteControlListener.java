package xdecoder.java;

import com.sun.glass.ui.Application;
import com.sun.glass.ui.Robot;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import javafx.application.Platform;
import javafx.stage.Screen;

public class RemoteControlListener
{
    private static final int SCREEN_HEIGHT = (int) Screen.getPrimary().getVisualBounds().getHeight();
    
    private static final int SCREEN_WIDTH = (int) Screen.getPrimary().getVisualBounds().getWidth();
    
    private static final Robot ROBOT = Application.GetApplication().createRobot();
    
    private static volatile boolean isRunning;
    
    private static Main CONTEXT;

    public RemoteControlListener() 
    {
        
    }

    public static void listen() 
    {
        if (isRunning) 
        {
            throw new IllegalStateException("Service is already running.");
        }
        
        CONTEXT = Main.ME;
        
        Runnable task = () -> 
        {
            isRunning = true;
            try {
                RemoteControlListener.doListen();
            } catch (Exception ex) {
                ErrorLogger.log((Throwable) ex);
            }
            isRunning = false;
            System.out.println("RCL Terminated!");
        };
        
        new Thread(task, "Remote Control Listener (via HDMI CEC)").start();
    }

    private static void doListen() throws Exception 
    {
        Process process = new ProcessBuilder("sudo", "cec-client").redirectErrorStream(true).start();
        
        try 
        (
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream())); 
            //PrintWriter writer = new PrintWriter(process.getOutputStream());
        )
        {
            while(isRunning && process.isAlive())
            {
                    String line = reader.readLine();
                    
                    if(line != null && line.toLowerCase().contains("pressed: ") && IntelliHop.START_TIME == 0){IntelliHop.start();}
                    else 
                    {
                        if(line != null && line.toLowerCase().contains("released: ") && IntelliHop.START_TIME != 0){IntelliHop.stop();}
                    }
                    
                    if (line != null && line.toLowerCase().contains("pressed: left")) 
                    {
                        moveMouseByX(-IntelliHop.X_PIXEL_HOP_VALUE());
                    }
                    else if (line != null && line.toLowerCase().contains("pressed: right")) 
                    {
                        RemoteControlListener.moveMouseByX(IntelliHop.X_PIXEL_HOP_VALUE());
                    }
                    else if (line != null && line.toLowerCase().contains("pressed: up")) 
                    {
                        RemoteControlListener.moveMouseByY(-IntelliHop.Y_PIXEL_HOP_VALUE());
                    }
                    else if (line != null && line.toLowerCase().contains("pressed: down")) 
                    {
                        RemoteControlListener.moveMouseByY(IntelliHop.Y_PIXEL_HOP_VALUE());
                    }
                    else if (line != null && line.toLowerCase().contains("released: stop")) 
                    {
                        CONTEXT.stopAllVideos();
                    }
                    else if (line != null && line.toLowerCase().contains("released: select")) 
                    {
                        mousePress(true);
                        mouseRelease(true);
                    
                        try{GPIOControl.PIN1.pinBlink(50, 50);}catch(Throwable e){ErrorLogger.log(e);}
                    }
            }
        }
    }

    private static void moveMouseByX(int x) 
    {
        Platform.runLater(() -> 
        {
            if (ROBOT.getMouseX() + x - 16 >= 0 && ROBOT.getMouseX() + x + 16 <= SCREEN_WIDTH) 
            {
                ROBOT.mouseMove(ROBOT.getMouseX() + x, ROBOT.getMouseY());
            } else if (ROBOT.getMouseX() + x - 16 < 0) {
                ROBOT.mouseMove(0, ROBOT.getMouseY());
            } else if (ROBOT.getMouseX() + x + 16 > SCREEN_WIDTH) {
                ROBOT.mouseMove(SCREEN_WIDTH - 16, ROBOT.getMouseY());
            }
            Main.ME.getCursor().update(ROBOT.getMouseX(), ROBOT.getMouseY());
        });
    }

    private static void moveMouseByY(int y) {
        Platform.runLater(() -> {
            if (ROBOT.getMouseY() + y - 16 >= 0 && ROBOT.getMouseY() + y + 16 <= SCREEN_HEIGHT) {
                ROBOT.mouseMove(ROBOT.getMouseX(), ROBOT.getMouseY() + y);
            } else if (ROBOT.getMouseY() + y - 16 < 0) {
                ROBOT.mouseMove(ROBOT.getMouseX(), 0);
            } else if (ROBOT.getMouseY() + y + 16 > SCREEN_HEIGHT) {
                ROBOT.mouseMove(ROBOT.getMouseX(), SCREEN_HEIGHT - 16);
            }
            Main.ME.getCursor().update(ROBOT.getMouseX(), ROBOT.getMouseY());
        }
        );
    }

    private static void mousePress(boolean left) {
        Platform.runLater(() -> {
            ROBOT.mousePress(left ? 1 : 2);
        }
        );
    }

    private static void mouseRelease(boolean left) 
    {
        Platform.runLater(() -> 
        {
            ROBOT.mouseRelease(left ? 1 : 2);
        }
        );
    }

    public void stopListening()
    {
        isRunning = false;
    }

    private static class IntelliHop 
    {
        final static long FAST_HOP_TIMEOUT = 1_000; //millis
                
        final static int XY_HOP_DEFAULT_VALUE = 28;
        
        final static int XY_HOP_FAST_VALUE = 56;
        
        static long START_TIME;
        
        static void start()
        {
            START_TIME = System.currentTimeMillis();
        }
        
        static void stop()
        {
            START_TIME = 0;
        }
        
        static int X_PIXEL_HOP_VALUE() 
        {
            boolean fast = START_TIME != 0 && System.currentTimeMillis() - START_TIME >= FAST_HOP_TIMEOUT;
            
            return fast?XY_HOP_FAST_VALUE:XY_HOP_DEFAULT_VALUE;
        }

        static int Y_PIXEL_HOP_VALUE() 
        {
            boolean fast = START_TIME != 0 && System.currentTimeMillis() - START_TIME >= FAST_HOP_TIMEOUT;
            
            return fast?XY_HOP_FAST_VALUE:XY_HOP_DEFAULT_VALUE;
        }
    }
}
