/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package xdecoder.java;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Screen;

/**
 *
 * @author TDavid
 */

public class ActivityArea extends AnchorPane
{
    private final TilePane TILES = new TilePane();
    
    private final Main context;
    
    private boolean isSetup;
    
    public ActivityArea()
    {
        context = Main.ME;
    }
    
    @Override
    protected void layoutChildren() 
    {
        if(!isSetup)
        {
            setup();
            
            isSetup = true;
        }
        
        super.layoutChildren();
    }
    
    public void setup()
    {
        setMinHeight(Screen.getPrimary().getVisualBounds().getHeight()-120);
        setMaxHeight(Screen.getPrimary().getVisualBounds().getHeight()-120);
        setHeight(Screen.getPrimary().getVisualBounds().getHeight()-120);
        setHeight(Screen.getPrimary().getVisualBounds().getHeight()-120);
        
        AnchorPane.setRightAnchor(TILES, 20.0);
        AnchorPane.setBottomAnchor(TILES, 0.0);
        AnchorPane.setLeftAnchor(TILES, 0.0);
        AnchorPane.setTopAnchor(TILES, 0.0);
        
        TILES.setAlignment(Pos.CENTER);
        TILES.setHgap(20);
        TILES.setVgap(20);
        
        for(int i = 1; i <= 24; i++)
        {
            CustomImage img = new CustomImage();
            
            img.setStyle("-fx-background-image: url('/xdecoder/assets/thumbnails/0"+i+".jpg');"
                    + "-fx-background-size: 100% 100%;");
            img.setMinSize(145, 145);
            img.setMaxSize(145, 145);
            
            _TEMP_customise(i, img);
            
            TILES.getChildren().add(img);
        }
        
        getChildren().add(TILES);
        
        isSetup = true;
    }

    private void _TEMP_customise(int number, CustomImage img) 
    {
        {
            switch(number)
            {
                case 1: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("450p7goxZqg");}); img.setText("All of Me"); break;
               
                case 2: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("kzzsBzgjA6A");}); img.setText("Soft Work"); break;
                
                case 3: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("8YhAFBOSk1U");}); img.setText("Pana"); break;
                
                case 4: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("SFmZKO35SfQ");}); img.setText("Bobo"); break;
                
                case 5: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("-5YRQyW89cY");}); img.setText("Big (1988)"); break;
                
                case 6: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("6Mgqbai3fKo");}); img.setText("Chantaje"); break;
                    
                case 7: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("2VwXnHHKE9M");}); img.setText("TETA Hits 2016"); break;
               
                case 8: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("34Na4j8AVgA");}); img.setText("The Weeknd"); break;
                
                case 9: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("G2rPZhaL1oQ");}); img.setText("Climax"); break;
                
                case 10: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("CQh-lKTf7QM");}); img.setText("Closer"); break;
                
                case 11: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("UqyT8IEBkvY");}); img.setText("Magic"); break;
                
                case 12: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("SXiSVQZLje8");}); img.setText("Side To Side"); break;
                    
                case 13: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("ANS9sSJA9Yc");}); img.setText("Don't Wanna Know"); break;
               
                case 14: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("euCqAq6BRa4");}); img.setText("Let Me Love You"); break;
                
                case 15: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("UprcpdwuwCg");}); img.setText("Heathens"); break;
                
                case 16: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("QpbQ4I3Eidg");}); img.setText("Bad Things"); break;
                
                case 17: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("WTEeDgO-EGs");}); img.setText("Cobra vs B.Mamba"); break;
                
                case 18: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("vFbjgKjK8YY");}); img.setText("Animal Fights"); break;
                    
                case 19: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("U89pc92MfuQ");}); img.setText("Giraffe vs Lion"); break;
               
                case 20: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("QC_PgpPXiTg");}); img.setText("Rhino vs Elephant"); break;
                
                case 21: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("3kzSuU_daLQ");}); img.setText("Cat vs Cobra"); break;
                
                case 22: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("BBgY6jpms0U");}); img.setText("Tiger vs Lion"); break;
                
                case 23: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("QEDUpIz2r_I");}); img.setText("Croc vs Shark"); break;
                
                case 24: img.setOnMouseClicked((MouseEvent event)
                        -> {context.playVideo("kOkQ4T5WO9E");}); img.setText("This Is What You Came For"); break;
                    
                default: img.setOnMouseClicked((MouseEvent event) 
                         -> {context.playVideo("kOkQ4T5WO9E");}); break;
            }
        }
    }
    
    private class CustomImage extends AnchorPane
    {
        Label text = new Label();
        
        boolean isCISetup;
        
        public CustomImage()
        {}
        
        public void setText(String t)
        {
            text.setText(t);
        }

        @Override
        protected void layoutChildren() 
        {
            if (!isCISetup) 
            {
                getChildren().add(text);
                
                text.setTextFill(Paint.valueOf(Color.WHITE.toString()));
                text.setTextOverrun(OverrunStyle.WORD_ELLIPSIS);
                text.setStyle("-fx-background-color: black");
                AnchorPane.setBottomAnchor(text, 0.0);
                text.setAlignment(Pos.BOTTOM_CENTER);
                text.setFont(Font.font("bold", 15));
                text.setMinSize(getMinWidth(), 30);
                text.setMaxSize(getMaxWidth(), 30);
                
                isCISetup = true;
            }
            
            super.layoutChildren();
        }
    }
}
