/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package xdecoder.java;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.chart.BubbleChart;
import javafx.scene.chart.Chart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import xdecoder.java.youtubeapi.YoutubeInterface;

/**
 *
 * @author TDavid
 */
public class NavigationMenu extends AnchorPane
{
    private final SearchBar SEARCH_BAR = new SearchBar();
    
    private boolean isSetup;
    
    @Override
    protected void layoutChildren() 
    {
        if(!isSetup)
        {
            setup();
            
            isSetup = true;
        }
        
        super.layoutChildren();
    }
    
    public NavigationMenu()
    {
        
    }
    
    public void setup()
    {
        setPrefHeight(Screen.getPrimary().getVisualBounds().getHeight()-120);
            
        Label bg = new Label();
            
        bg.setMinSize(getPrefWidth(), getPrefHeight());
        bg.setStyle("-fx-background-color: black;");
        bg.setOpacity(0.75);
        
        setOnMouseExited((MouseEvent event) ->
        {
            new Thread(()->
            {
                try{Thread.sleep(2);}catch(Exception e){ErrorLogger.log(e);}
                
                if(!OnscreenKeyboard.isMouseFocused)
                {
                    Main.ME.hideKeyboard();
                    
                    Platform.runLater(()->{setVisible(false);});
                }
            }).start();
        });
        
        AnchorPane.setRightAnchor(SEARCH_BAR, 15.0);
        AnchorPane.setTopAnchor(SEARCH_BAR, 50.0);
        SEARCH_BAR.setPrefSize(400, 40);
        
        getChildren().add(bg);
        getChildren().addAll(getListItems());
        
        getChildren().addAll(SEARCH_BAR, getChart());
        
        isSetup = true;
    }
    
    public void toggle()
    {    
        setVisible(!isVisible());
        
        Main.ME.hideKeyboard();
                
       //NodeSlider.slideX(this, getLayoutX() < 0);
    }
    
    private Chart getChart()
    {
        final NumberAxis xAxis = new NumberAxis(1, 53, 4);
        final NumberAxis yAxis = new NumberAxis(0, 80, 10);
        
        final BubbleChart<Number, Number> chart = new BubbleChart<>(xAxis, yAxis);
        
        xAxis.setTickLabelFill(Color.WHITE);
        
        yAxis.setTickLabelFill(Color.WHITE);
        
        //yAxis.setLabel("Product Budget");
        
        //xAxis.setLabel("Week");
        
        //chart.setTitle("Usage stats");
        //chart.setStyle("-fx-text-fill: red;");
        
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Frequency");
        series1.getData().add(new XYChart.Data(3, 35));
        series1.getData().add(new XYChart.Data(12, 60));
        series1.getData().add(new XYChart.Data(15, 15));
        series1.getData().add(new XYChart.Data(22, 30));
        series1.getData().add(new XYChart.Data(28, 20));
        series1.getData().add(new XYChart.Data(35, 41));
        series1.getData().add(new XYChart.Data(42, 17));
        series1.getData().add(new XYChart.Data(49, 30));         
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Time spent");
        series2.getData().add(new XYChart.Data(8, 15));
        series2.getData().add(new XYChart.Data(13, 23));
        series2.getData().add(new XYChart.Data(15, 45));
        series2.getData().add(new XYChart.Data(24, 30));
        series2.getData().add(new XYChart.Data(38, 78));
        series2.getData().add(new XYChart.Data(40, 41));
        series2.getData().add(new XYChart.Data(45, 57));
        series2.getData().add(new XYChart.Data(47, 23));
        
        chart.getData().addAll(series1, series2);
        
        AnchorPane.setBottomAnchor(chart, 5.0);
        AnchorPane.setRightAnchor(chart, 5.0);
        
        return chart;
    }
    
    private Node[] getListItems()
    {
        Node[] items = new Node[7];
        
        for(int i = 0; i < items.length; i++)
        {
            Label item = new Label();
            
            item.setMinWidth(getPrefWidth()-(SEARCH_BAR.getPrefWidth()+92));
            item.setMaxWidth(getPrefWidth()-(SEARCH_BAR.getPrefWidth()+92));
            AnchorPane.setTopAnchor(item, (double)((i*45) +50));
            item.setTextOverrun(OverrunStyle.ELLIPSIS);
            item.setPadding(new Insets(0, 0, 30, 51));
            item.setFont(Font.font("monospace", 18));
            item.getStyleClass().add("nav-list-item");
            item.setAlignment(Pos.TOP_LEFT);
            item.setUserData(i+"");
            item.setMaxHeight(55);
            item.setMinHeight(30);
            
            switch(i)
            {
                case 0: item.setText("Oprah & Michele Obama"); item.setOnMouseClicked((MouseEvent e)->{Main.ME.playVideo("_x7y-KAiP0I");}); break;
                
                case 1: item.setText("Obasanjo on BBC");
                 item.setOnMouseClicked((MouseEvent e)->{Runnable runnable = () -> 
                {
                    try 
                    {
                        if(Utils.isNetworkConnected(true))
                        {
                            Main.ME.toast(Strings.SEARCH_QUERY_BEING_PROCCESSED, 15_000);
                        
                            Main.ME.playVideo(YoutubeInterface.getVideoID("Obasanjo on BBC HardTalk with steven sackur"));
                        }
                        else
                        {
                            Main.ME.toast(Strings.NO_NETWORK_CONNECTION, null);
                        }
                    } catch (Exception ex){ErrorLogger.log(ex); Main.ME.toast(Strings.FAILED_SEARCH_QUERY, null);}
                };
                
                new Thread(runnable).start();});break;
                
                case 2: item.setText("The Story Of Earth"); item.setOnMouseClicked((MouseEvent e)->{Main.ME.playVideo("d0z9x55EGh8");}); break;
                
                case 3: item.setText("Antarctica's secret"); item.setOnMouseClicked((MouseEvent e)->{Main.ME.playVideo("PXDUQd1l_h8");}); break;
                
                case 4: item.setText("AL Jazeera Live"); item.setOnMouseClicked((MouseEvent e)->{Main.ME.playVideo("1YnlX4CkPDY");}); break;
                
                case 5: item.setText("Fox News Live"); item.setOnMouseClicked((MouseEvent e)->{Main.ME.playVideo("y60wDzZt8yg");}); break;
                    
                case 6: item.setText("Drunk in Love"); item.setOnMouseClicked((MouseEvent e)->{Main.ME.playVideo("p1JPKLa-Ofc");}); break;
            }
            
            items[i] = item;
        }
        
        return items;
    }
}