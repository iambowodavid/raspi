/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xdecoder.java;

import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.scene.layout.Pane;

/**
 *
 * @author TDavid
 */
public class NodeSlider 
{
    public static void slideX(Pane node, boolean toTheRight)
    {
        final int INTERVAL_MILLIS = 1;
        
        final double hop = node.getPrefWidth()-1;
        
        if(toTheRight){node.setLayoutX(-node.getPrefWidth());}
        
        final TimerTask[] task = {null};
        
        final Runnable run = () ->
        {
            double x = node.getLayoutX();
            
            double nx = toTheRight ?x+hop :x-hop;
            
            if(toTheRight && nx >= 0)
            {
                node.setLayoutX(0);
                
                task[0].cancel();
                
                return;
            }
            else if(!toTheRight && nx <= -node.getPrefWidth())
            {
                task[0].cancel();
                
                return;
            }
            
            node.setLayoutX(nx);
        };
        
        task[0] = new TimerTask()
        {
            @Override
            public void run() 
            {
                Platform.runLater(run);
            }
        };
        
        new Timer().schedule(task[0], 0, INTERVAL_MILLIS);
    }
}
