/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package xdecoder.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author and copyright Bowofoluwa Okeowo
 */

public class Main extends Application implements Initializable
{
    @FXML
    private Label progress_bar;
    
    @FXML
    private Label bg_toggle_btn;
    
    @FXML
    private Label nav_toggle_btn;
    
    @FXML
    private NavigationMenu navigation_menu;
    
    @FXML
    private OnscreenKeyboard ONSCREEN_KEYBOARD;
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        bg_toggle_btn.setOnMouseClicked((MouseEvent event) ->
        {
            try 
            {
                //Runtime.getRuntime().exec("sudo /bin/sh -c \"sleep 5 java -jar /boot/dobox/storage/Y/dobox/BrickBreaker-RPI.jar\" &");
                
                //Utils.terminate(0);
            } catch (Exception ex) {ErrorLogger.log(ex);}
        });
        
        //Navigation munu toggle button
        nav_toggle_btn.setOnMouseClicked((MouseEvent event) ->
        {
            {
                navigation_menu.toggle();
            }
        });
    }
    
    public static Main ME;
    
    private Stage stage;

    private Scene scene;
    
    @SuppressWarnings("LeakingThisInConstructor")
    public Main()
    {
        Main.ME = this;
    }
    
    @Override
    public void start(Stage S) throws Exception 
    {
        //Mini-Splash
        {
            Label root = new Label();
            
            root.setStyle("-fx-background-color: black;"
                    + "-fx-background-repeat: no-repeat;"
                    + "-fx-background-position: center center;"
                    + "-fx-background-image: url('/xdecoder/assets/boot-logo.png');");
        
            scene = new Scene(root);
            
            try{GPIOControl.PIN1.pinBlink(100, 12_000);}catch(Throwable e){ErrorLogger.log(e);}
        }
        // END-Mini-Splash
        
        scene.setFill(Paint.valueOf(Color.TRANSPARENT.toString()));
        scene.getStylesheets().add("/xdecoder/css/general-stylesheet.css");
        
        stage = S;
        stage.setHeight(Screen.getPrimary().getVisualBounds().getHeight());
        stage.setWidth(Screen.getPrimary().getVisualBounds().getWidth());
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setScene(scene);
        
        stage.show();
        
        Platform.setImplicitExit(false);
        
        closeOMXPlayer();
        
        final Runnable task = () ->
        {
            try
            {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/xdecoder/fxml/Main.fxml"));
                
                loader.setController(Main.this);
                
                scene.setRoot(loader.load());
                
                Runnable runnable = () -> 
                {
                    playStartupSound();
                        
                    startCommandListening();
                        
                    startServer();
                        
                    prestartBrowser();
                };
                
                new Timer().schedule(new TimerTask(){@Override public void run(){Platform.runLater(runnable);}}, 500);
            }catch(IOException ex){ErrorLogger.severeLog(ex);}
        };
        
        new Timer().schedule(new TimerTask(){@Override public void run(){Platform.runLater(task);}}, 1);
    }

    private void startCommandListening() 
    {
        scene.setOnKeyReleased((KeyEvent event) -> 
        {
            if(event.isControlDown() && event.getCode().equals(KeyCode.X))
            {
                stopStartupSound();
                
                Utils.terminate(0);
            }
            else if(event.isControlDown() && event.getCode().equals(KeyCode.S))
            {
                stopAllVideos();
            }
        });
        
        RemoteControlListener.listen();
    }
    
    private boolean isDisplayVisible()
    {
        return scene.getRoot().isVisible();
    }
    
    private void showDisplay()
    {
        Runnable run = () -> 
        {
            scene.getRoot().setVisible(true);
        };
        
        Platform.runLater(run);
    }
    
    private void hideDisplay()
    {
        Runnable run = () ->
        {
            {
                scene.getRoot().setVisible(false);
            }
        };
        
        Platform.runLater(run);
    }
    
    public Cursor getCursor()
    {
        return (Cursor)scene.lookup("#CURSOR");
    }
    
    private void closeOMXPlayer() 
    {
        try 
        {
            Runtime.getRuntime().exec("sudo killall omxplayer.bin");
        } catch (Exception e){ErrorLogger.log(e);}
    }

    private void playStartupSound() 
    {
        try{Runtime.getRuntime().exec("sudo omxplayer --loop --no-osd "+Utils.ROOT_DIRECTORY+"/play.mp3");}
        catch(Exception e){ErrorLogger.log(e);}
    }
    
    public void stopStartupSound() 
    {
        try 
        {
            Runtime.getRuntime().exec("sudo killall omxplayer.bin");
        } catch (Exception e){ErrorLogger.log(e);}
    }
    
    public void playVideo(final String id)
    {
        if(!Utils.isNetworkConnected(true))
        {
            Main.ME.toast(Strings.NO_NETWORK_CONNECTION, null);
            
            return;
        }
        
        Runnable runnable = () -> 
        {
            try 
            {
                stopStartupSound();
                
                Runtime.getRuntime()
                        .exec("sudo epiphany-browser -a --profile /root/.config http://localhost/YTE_"+id);
            } catch (Exception e){ErrorLogger.log(e);}
        };
        
        new Thread(runnable).start();
        
        Platform.runLater(() -> {hideDisplay();});
    }
    
    public void stopAllVideos()
    {
        try
        {
            stopStartupSound();
            
            Runtime.getRuntime().exec("sudo killall epiphany-browser");
        }catch(Exception e){ErrorLogger.log(e);}
        
        playStartupSound();
        
        Platform.runLater(() -> {showDisplay();});
    }
    
    private void startServer() 
    {
        @SuppressWarnings("ResultOfObjectAllocationIgnored")
        Runnable runnable = () ->
        {
            try 
            {
                ServerSocket server = new ServerSocket(80);
                
                for(;;)
                {
                    final Socket socket = server.accept();
                    
                    new Thread(
                    () -> {try{new XServer(socket);}
                    catch(Exception ex){if(!ex.getMessage().contains("onnection reset")){ErrorLogger.severeLog(ex);}}}
                    ).start();
                }
            } catch (Exception ex){ErrorLogger.severeLog(ex);}            
        };
        
        new Thread(runnable).start();
    }

    private void prestartBrowser() 
    {
        Runnable runnable = () -> 
        {
            try 
            {
                Runtime.getRuntime()
                        .exec("sudo epiphany-browser -a --profile /root/.config about:blank");
                
                new Timer().schedule(new TimerTask()
                {@Override public void run()
                {try{Runtime.getRuntime().exec("sudo killall epiphany-browser");}
                catch(Exception e){ErrorLogger.log(e);}}}, 3_000);
            } catch (Exception e){ErrorLogger.log(e);}
        };
        
        new Thread(runnable).start();
    }
    
    public void showKeyboard(TextField field)
    {
        Platform.runLater(() -> {ONSCREEN_KEYBOARD.setTextField(field); ONSCREEN_KEYBOARD.setVisible(true);});
    }
    
    public void hideKeyboard()
    {
        Platform.runLater(() -> {ONSCREEN_KEYBOARD.setTextField(null); ONSCREEN_KEYBOARD.setVisible(false);});
    }
    
    public void toast(String msg, final Integer period)
    {
        Runnable runnable = () -> 
        {
            Label label = new Label(msg);
            
            label.setId("toast__");
            label.setMinWidth(400);
            label.setLayoutX(500);
            label.setLayoutY(100);
            label.setWrapText(true);
            label.setFont(Font.font("bold", 21));
            label.setStyle("-fx-background-color: black;");
            label.setTextFill(Paint.valueOf(Color.WHITE.toString()));
            
            Pane root = ((Pane) scene.getRoot());
            
            Node oldToast = root.lookup("#"+label.getId());
            
            if(oldToast != null)
            {root.getChildren().remove(oldToast);}
            
            root.getChildren().add(label);
            
            new Timer().schedule(new TimerTask()
            {@Override public void run(){Platform.runLater(()->{root.getChildren().remove(label);});}}, period==null?10_000:period);
        };
        
        Platform.runLater(runnable);
    }

    public void showProgressBar() 
    {
        Runnable runnable = () -> 
        {
            progress_bar.setVisible(true);
        };
        
        Platform.runLater(runnable);
    }

    public void hideProgressBar()
    {
        Runnable runnable = () -> 
        {
            progress_bar.setVisible(false);
        };
        
        Platform.runLater(runnable);
    }
    
    private class XServer 
    {
        public XServer(Socket socket) throws Exception 
        {
            //try 
            {
                {
                    try
                        (
                            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                            PrintWriter writer = new PrintWriter(socket.getOutputStream())
                        ) 
                    {
                        String requestLine;

                        while ((requestLine = reader.readLine()) != null) 
                        {
                            if (requestLine.contains("video-ended")) 
                            {
                                socket.close();

                                Main.this.stopAllVideos();

                                break;
                            }
                            else if (requestLine.contains("video-ready")) 
                            {
                                socket.close();

                                Main.this.closeOMXPlayer();

                                break;
                            } 
                            else if (requestLine.contains("YTE_")) 
                            {
                                requestLine = requestLine.substring(requestLine.indexOf("YTE_") + 4);
                                requestLine = requestLine.split(" ")[0];
                                
                                String data = Utils.readResourceAsString("/xdecoder/assets/youtube-embedded.html").replace("YTE_VIDEO_ID", requestLine);
                                
                                writer.write("HTTP/1.1 200 OK\r\n");
                                writer.write("Server: XDecoder Server 1 Series\r\n");
                                writer.write("Content-Type: text/html\r\n");
                                writer.write("Content-Length: "+data.length()+"\r\n");
                                writer.write("Connection: close\r\n");
                                writer.write("\r\n");
                                writer.write(data+"\r\n");
                                
                                //writer.flush();
                                
                                //socket.close();

                                break;
                            }
                        }
                    }
                }
            }// catch (Exception ex) {ErrorLogger.severeLog(ex);}
        }
    }
    
    public static void main(String ... args){launch(args);}
}
